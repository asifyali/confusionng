import { Component, OnInit , Input, Inject} from '@angular/core';
import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';

import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import 'rxjs/add/operator/switchMap';

import { Http, Response } from '@angular/http';
import { baseURL } from '../shared/baseurl';
import { ProcessHttpmsgService } from '../services/process-httpmsg.service';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Comment } from '../shared/comment';
 
import { flyInOut, expand, visibility } from '../animations/app.animations';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.css'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
    },
    animations: [
      flyInOut(),
      visibility(),
      expand()
    ]
})

export class DishdetailComponent implements OnInit {
  
    dish: Dish;
    dishcopy = null;
    
    dishIds: number[];
    prev: number;
    next: number;
    commentsForm: FormGroup;
    comment: Comment;

    visibility = 'shown';

    constructor(private dishservice: DishService,
      private route: ActivatedRoute,
      private location: Location, @Inject('BaseURL') private BaseURL, private fb: FormBuilder) { 
        this.createForm();
      }
  


  ngOnInit() {
    this.dishservice.getDishIds().subscribe(dishIds => this.dishIds = dishIds);

    this.route.params
    .switchMap((params: Params) => { this.visibility = 'hidden'; return this.dishservice.getDish(+params['id']); })
    .subscribe(dish => { this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish.id); this.visibility = 'shown'; }
    );
  }

  setPrevNext(dishId: number) {
    let index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1)%this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1)%this.dishIds.length];
  }


     createForm() {
         this.commentsForm = this.fb.group({
          rating: 5,
          comment: '',
          author: ''
         });
      }
      

  onSubmit() { 
    this.comment = this.commentsForm.value;
    console.log(this.comment);
    this.commentsForm.reset();
    this.dishcopy.comments.push(this.comment);
    this.dishcopy.save()
      .subscribe(dish => { this.dish = dish; this.dishcopy = dish; console.log(this.dish); });
      
  }
    goBack(): void {
      this.location.back();
    }
  
  }
